﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a single card with suit and rank.
/// </summary>
public class Card 
{
    private const int NUM_RANKS = 13;
    private const int NUM_SUITS = 4;

    // value: number of the card: 1 and 14 are Ace, 11 is Jack, 12 is Queen, 13 is King
    [Range(1, NUM_RANKS + 1)]
    public int rank;

    // suit of the card
    public enum CardSuit { Hearts, Diamonds, Spades, Clubs };
    public CardSuit suit;

    public override string ToString()
    {
        string s1, s2;

        if (rank == 1 || rank == 14) s1 = "Ace";
        else if (rank == 11) s1 = "Jack";
        else if (rank == 12) s1 = "Queen";
        else if (rank == 13) s1 = "King";
        else s1 = rank.ToString();

        s2 = suit.ToString();

        return s1 + " of " + s2;
    }
}
