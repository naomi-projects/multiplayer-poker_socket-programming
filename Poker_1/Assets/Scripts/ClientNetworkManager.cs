﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Net;
using UnityEngine.UI;

#pragma warning disable 0649

public class ClientNetworkManager : MonoBehaviour
{
    // used to pass socket and buffer data to delegate
    class StateObject
    {
        internal byte[] buffer;
        internal Socket socket;
        internal StateObject(int size, Socket sock)
        {
            buffer = new byte[size];
            socket = sock;
        }
        internal StateObject(byte[] b, Socket sock)
        {
            buffer = b;
            socket = sock;
        }
    }
    
    #region Network Variables
    // port number
    private const int port = 2001;

    // maximums bytes to send between client and server
    private const int MAX_BYTES = 1024; // 1 byte per ASCII character including spaces

    public string serverName;

    public const string naomiDesktop = "DESKTOP-TK378AO";

    public string myIP;

    #endregion
    
    // for developers... 
    // Set to true to show log messages in the console using Display()
    public bool showDebugLog = true;

    private void Start()
    {
        myIP = Dns.GetHostName();
    }

    public void SendRequest(string data)
    {
        SendRequest(naomiDesktop, data);
    }

    /// <summary>
    /// Send data to the server.
    /// Preconditions: The server should be set up for communication and listening for new connections.
    /// </summary>
    /// <param name="data">A request being sent to the server.</param>
    public void SendRequest(string server, string data)
    {
        // make data buffer from the string input
        byte[] dataBuffer = Encoding.ASCII.GetBytes(data);

        // get server IP address
        //IPAddress serverIP = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0];
        //IPAddress serverIP = Dns.GetHostEntry(serverName).AddressList[0];        
        IPAddress serverIP = Dns.GetHostEntry(server).AddressList[0];

        // make network endpoint
        IPEndPoint clientEndPoint = new IPEndPoint(serverIP, port);

        // make new socket
        Socket clientSocket = new Socket(serverIP.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

        // create object to hold info for the clientSocket and data buffer
        StateObject stateObject = new StateObject(dataBuffer, clientSocket);

        // asynchronous connect to the server
        IAsyncResult asyncConnect = clientSocket.BeginConnect(
            clientEndPoint, 
            new AsyncCallback(ConnectCallback),
            stateObject        // passed to ConnectCallback
            );
        Display("Connecting to server...");
    }

    /// <summary>
    /// This is called from SendRequest to handle creating a TCP connection to the server.
    /// </summary>
    /// <param name="asyncConnect"></param>
    private static void ConnectCallback(IAsyncResult asyncConnect)
    {
        // get the socket and data buffer
        StateObject stateObject = (StateObject)asyncConnect.AsyncState;

        // end the asynchronous connect
        stateObject.socket.EndConnect(asyncConnect);

        if (stateObject.socket.Connected == false)
        {
            Debug.LogError("Client is not connected.");
            return;
        }
        Debug.Log("Client is connected.");       

        // asynchronous send the data to the server
        IAsyncResult asyncSend = stateObject.socket.BeginSend(
            stateObject.buffer,
            0,
            stateObject.buffer.Length,
            SocketFlags.None,
            new AsyncCallback(SendCallback),
            stateObject.socket      // Socket passed to SendCallback
            );
        Debug.Log("Sending data: " + stateObject.buffer);
    }

    /// <summary>
    /// This is called from ConnectCallback to handle sending data to the server.
    /// </summary>
    /// <param name="asyncSend"></param>
    private static void SendCallback(IAsyncResult asyncSend)
    {
        // create a new socket for the connection
        Socket clientSocket = (Socket)asyncSend.AsyncState;

        // end the pending asynchronous send, get number of bytes sent
        int bytesSent = clientSocket.EndSend(asyncSend);
        Debug.Log("Bytes sent: " + bytesSent.ToString());

        // create object to hold info for the clientSocket and data buffer
        StateObject stateObject = new StateObject(MAX_BYTES, clientSocket);

        // begin asynchronous receive data from the server
        // store the data in stateObject.buffer, get maximum bytes to fill the buffer
        // invokes ReceiveCallback when the operation is complete
        IAsyncResult asyncReceive = clientSocket.BeginReceive(
            stateObject.buffer,
            0,
            stateObject.buffer.Length,
            SocketFlags.None,
            new AsyncCallback(ReceiveCallback),
            stateObject         // StateObject passed to ReceiveCallback
            );
        Debug.Log("Receiving data...");
        // Timeout(asyncReceive);
    }

    /// <summary>
    /// This is called from SendCallback to handle receiving a server response to sent data.
    /// Calls ClientGameManager.DoAction() with the server response as the passed argument.
    /// </summary>
    /// <param name="asyncReceive"></param>
    private static void ReceiveCallback(IAsyncResult asyncReceive)
    {
        // get the socket and data buffer
        StateObject stateObject = (StateObject)asyncReceive.AsyncState;

        // end the pending asynchronous read and get the number of bytes received
        int bytesReceived = stateObject.socket.EndReceive(asyncReceive);

        // get the string sent by the server
        string response = Encoding.ASCII.GetString(stateObject.buffer);
        Debug.Log("Data received: " + response);

        //? do action on the client based on the server response
        ClientGameManager gameMgr = FindObjectOfType<ClientGameManager>();
        gameMgr.ProcessResponse(response);

        // close the connection
        stateObject.socket.Shutdown(SocketShutdown.Both);
        stateObject.socket.Close();
    }
    
    // time out after 2 seconds
    internal static bool Timeout(IAsyncResult ar)
    {
        int i = 0;
        while (ar.IsCompleted == false)
        {
            if (i++ > 20)
            {
                Debug.LogError("Timed out.");
                return false;
            }
            // check every 100 milliseconds
            Thread.Sleep(100);
        }
        return true;
    }

    // display string on console and on UI display
    private void Display(string s)
    {
        if (showDebugLog)
            Debug.Log(s);
    }
}
