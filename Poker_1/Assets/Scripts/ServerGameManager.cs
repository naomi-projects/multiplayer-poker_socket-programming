﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerGameManager : MonoBehaviour
{
    // reference to the other server scripts in this scene
    private ServerUIController serverUICtrl;
    private ServerNetworkManager serverNetMgr;

    #region Constants
    // constants    
    public const int NUM_RANKS = 13;
    public const int NUM_SUITS = 4;
    public const int HAND_SIZE = 5;
    public const int MAX_PLAYERS = 5;

    // value of hands
    private const int ROYAL_FLUSH = 900;
    private const int STRAIGHT_FLUSH = 800;
    private const int FOUR_OF_A_KIND = 700;
    private const int FULL_HOUSE = 600;
    private const int FLUSH = 500;
    private const int STRAIGHT = 400;
    private const int THREE_OF_A_KIND = 300;
    private const int TWO_PAIR = 200;
    private const int ONE_PAIR = 100;
    private const int HIGH_CARD = 0;

    #endregion

    #region Game Management Variables
    /// <summary>
    /// Represents a players id number and hand of cards.
    /// </summary>
    private class Player
    {
        public int number;
        public Card[] cards;
    }

    // deck holds 52 cards, one of each value for each suit
    private Card[,] completeDeck;

    // deck holds 52 cards, one of each value for each suit
    private Card[] currentDeck;

    // array index of the top card of the deck
    private int topOfDeck = 0;

    // players
    private Player[] players;
    
    // game process status
    public enum GameStatus { off, inlobby, pregame, ingame, postgame }
    private GameStatus status = GameStatus.off;
    // off:     server is off, game has not yet been created
    // inlobby: server is on, game has been created with name and password, 
    //          server-client now in lobby waiting for players to connect and ready up
    // pregame: server pressed start from the lobby, now in game but game has not started
    // ingame:  game is now in process
    // postgame:game is in wrap up, i.e. picking winners for the game, showing endgame stuff 
           
    /// <summary>
    /// Set the game status.
    /// </summary>
    /// <param name="s"></param>
    public void SetStatus(GameStatus s)
    {
        status = s;
    }

    // game name and password.
    private string gameName;
    private string password;

    private string gameOwner = "gameOwner#1";

    #endregion

    #region Player Information
    private string[] playerNames;       // the player name (string)
    private bool[] humanPlayers;        // true if human player, false if AI
    private int numHumanPlayers;    // total number of human players in this game
    private bool[] playersReady;        // true if player readied up, true for AIs always

    #endregion

    #region Server-Client Variables
    // server-client is always Player 1 (lobby leader)
    private const int playerNum = 0;

    #endregion

    /// <summary>
    /// Start
    /// </summary>
    private void Start()
    {
        serverUICtrl = FindObjectOfType<ServerUIController>();
        serverNetMgr = FindObjectOfType<ServerNetworkManager>();

        humanPlayers = new bool[ServerGameManager.MAX_PLAYERS];
        playerNames = new string[ServerGameManager.MAX_PLAYERS];
        playersReady = new bool[ServerGameManager.MAX_PLAYERS];
    }

    #region CreateGame Functions
    /// <summary>
    /// Called when server creates the game. Set the game name and password.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="pass"></param>
    public void CreateGame(string name, string pass)
    {
        gameName = name;
        password = pass;

        // server-client is player 0
        humanPlayers[0] = true;
        playersReady[0] = true;
        playerNames[0] = gameOwner;
        numHumanPlayers = 1;
        serverUICtrl.SetLobbyUI_PlayerName(playerNames[0], 0);
    }
    #endregion

    #region Lobby Functions

    /// <summary>
    /// Adds a new player to the game, saves their information.
    /// </summary>
    /// <param name="name"></param>
    /// <returns>-1 if error in adding player, id number if player successfully added.</returns>
    public int AddPlayer(string name)
    {
        // at maximum players so do not add the player
        if (numHumanPlayers >= ServerGameManager.MAX_PLAYERS) return -1;
        int number = 0;
        // find a number for the new player
        for (int i = 1; i < MAX_PLAYERS; i++)
        {
            if (humanPlayers[i] == false)
            {
                number = i;
                break;
            }
        }
        // no number available, all are human players
        if (number == 0) return -1;

        // add the player with their number and name
        playerNames[number] = name;
        humanPlayers[number] = true;
        numHumanPlayers++;

        // set the UI for the player connected
        serverUICtrl.SetLobbyUI_PlayerName(name, number);

        return number;
    }

    /// <summary>
    /// Remove a player from the lobby.
    /// </summary>
    /// <param name="number"></param>
    /// <returns>Returns true if the player existed and is now removed.</returns>
    public bool RemovePlayer(int number)
    {
        // number must be 1 through 4 
        if (number < 1 || number > MAX_PLAYERS - 1) return false;
        // check if trying to remove player that isn't there
        if (humanPlayers[number] == false) return false;

        // remove human player
        humanPlayers[number] = false;
        // reset their name
        playerNames[number] = null; // = ""; 
        // update the number of human players
        numHumanPlayers--;

        // reset the UI for the player disconnected
        serverUICtrl.SetLobbyUI_PlayerName("AI", number);

        return true;
    }

    /// <summary>
    /// Ready or unready a player.
    /// </summary>
    /// <param name="ready"></param>
    /// <param name="number"></param>
    /// <returns></returns>
    public bool ReadyPlayer(bool ready, int number)
    {
        // number must be 1 through 4 
        if (number < 1 || number > MAX_PLAYERS - 1) return false;

        // set the lobby UI ready icon for the player
        serverUICtrl.SetLobbyUI_PlayerReady(ready, number);
        return true;
    }

    /// <summary>
    /// Set up the deck and set up the players. Called from StartGame in ServerUIController.
    /// </summary>
    public void StartGame()
    {
        // set the game status
        status = GameStatus.pregame;

        // Set up the decks
        completeDeck = new Card[NUM_SUITS, NUM_RANKS + 1];
        currentDeck = new Card[NUM_SUITS * NUM_RANKS];
        for (int i = 0; i < NUM_SUITS; i++)
            for (int j = 1; j < NUM_RANKS + 1; j++)
            {
                // create a new card with suit and value, and store it in the deck
                completeDeck[i, j] = new Card()
                {
                    suit = (Card.CardSuit)i,
                    rank = j
                };
                // create a new card with suit and value, and store it in the deck
                currentDeck[(i * NUM_RANKS) + j - 1] = completeDeck[i, j];
            }

        // Set up the players
        players = new Player[MAX_PLAYERS];
        for (int k = 0; k < MAX_PLAYERS; k++)
        {
            players[k] = new Player()
            {
                number = k,
                cards = new Card[HAND_SIZE]
            };
        }

        // start a round
        ShuffleAndDeal();
    }
    #endregion

    #region Gameplay Functions

    /// <summary>
    /// Start a round of poker. Create and shuffle the deck, deal 5 cards to each player.
    /// </summary>
    public void ShuffleAndDeal()
    {
        status = GameStatus.ingame;

        // Shuffle the current deck
        Shuffle(NUM_SUITS * NUM_RANKS * 10);

        // Deal a hand to each player
        DealCardsToAll(MAX_PLAYERS, HAND_SIZE);
    }
    
    /// <summary>
    /// Deal a round of cards to each player.
    /// </summary>
    /// <param name="numPlayers">Number of players to deal to.</param>
    /// <param name="numCards">Number of cards to deal to each player.</param>
    public void DealCardsToAll(int numPlayers, int numCards)
    {
        // number of cards to deal to each player
        for (int cardNum = 0; cardNum < numCards; cardNum++)
        {
            for (int playerNum = 0; playerNum < numPlayers; playerNum++)
            {
                // deal the card to the player
                players[playerNum].cards[cardNum] = DealCard();
            }
        }
        Card[] temp = SetAcesHigh(players[0].cards);
        temp = SortByRank(temp);
        for (int i = 0; i < 5; i++)
        {
            serverUICtrl.SetCardUI(players[0].number, i, temp[i]);
        }
    }

    /// <summary>
    /// Called by ServerUIController to reveal all the players' cards.
    /// </summary>
    public void ShowAllCards()
    {
        foreach (Player player in players)
        {
            Card[] temp = SetAcesHigh(player.cards);
            temp = SortByRank(temp);
            for (int i = 0; i < 5; i++)
            {
                serverUICtrl.SetCardUI(player.number, i, temp[i]);
            }
        }
    }

    /// <summary>
    /// Get the winners of the round. Compare the 5-card hands of each 6 players, display the winners.
    /// </summary>
    public List<int> EndRound()
    {
        // Get the winning players' numbers
        return GetWinningHand();
    }

    //? done -- don't touch
    /// <summary>
    /// Reset the current deck to be all 52 cards in order.
    /// </summary>
    public void ResetCurrentDeck()
    {
        for (int i = 0; i < NUM_SUITS; i++)
            for (int j = 1; j < NUM_RANKS + 1; j++)
            {
                currentDeck[(i * NUM_RANKS) + j - 1] = completeDeck[i, j];
            }
    }

    //? done -- don't touch
    /// <summary>
    /// Randomizes the order of cards in the currentDeck.
    /// </summary>
    /// <param name="n">The number of 2 card swaps to perform.</param>
    public void Shuffle(int n)
    {
        // iterate n times
        for (int i = 0; i < n; i++)
        {
            // select 2 random indexes
            int a = UnityEngine.Random.Range(0, NUM_SUITS * NUM_RANKS);
            int b = UnityEngine.Random.Range(0, NUM_SUITS * NUM_RANKS);
            // switch the cards at those indexes
            Card temp = currentDeck[a];
            currentDeck[a] = currentDeck[b];
            currentDeck[b] = temp;
        }
    }

    //? done -- don't touch
    /// <summary>
    /// Takes a card off the top of the currentDeck.
    /// </summary>
    /// <returns>The Card taken from the top of the deck.</returns>
    public Card DealCard()
    {
        // index of top card should be from 0 to 51
        if (topOfDeck < NUM_SUITS * NUM_RANKS && topOfDeck >= 0)
        {
            // return the card and change the pointer to the next card
            return currentDeck[topOfDeck++];
        }
        // index out of range
        Debug.LogError("Top card index out of range.");
        return null;
    }

    //? done -- don't touch
    /// <summary>
    /// Compares the hands of all 5 players to determine the winner(s).
    /// Precondition: all 5 players have 5 cards
    /// </summary>
    /// <returns>A list of integers of the winning player numbers.</returns>
    private List<int> GetWinningHand()
    {
        // check each hand against each other hand
        List<int> winners = new List<int>();

        // check 1 v 2
        int firstCheck = CompareTwoHands(players[0].cards, 0, players[1].cards, 1, 0);
        // check 3 v 4
        int secondCheck = CompareTwoHands(players[2].cards, 2, players[3].cards, 3, 2);

        // check firstCheck v secondCheck : (1 v 2) v (3 v 4)
        int thirdCheck = CompareTwoHands(players[firstCheck].cards, firstCheck, players[secondCheck].cards, secondCheck, firstCheck);

        // check thirdCheck vs Player5 : ((1 v 2) v (3 v 4)) v 5 
        int winningPlayer = CompareTwoHands(players[thirdCheck].cards, thirdCheck, players[4].cards, 4, thirdCheck);

        // add the winning player number to the winners
        winners.Add(winningPlayer);

        // check the winning hand for ties with all other hands
        for (int i = 0; i < MAX_PLAYERS; i++)
        {
            // don't check the winning player hand against itself
            if (i != winningPlayer)
            {
                // add the player number to the winners if there is a tie
                if (CompareTwoHands(players[winningPlayer].cards, winningPlayer, players[i].cards, i, -1) == -1)
                    winners.Add(i);
            }
        }
        return winners;
    }

    //? done -- don't touch
    /// <summary>
    /// Returns a string representation of a specified player's hand of cards.
    /// </summary>
    /// <param name="playerNum"></param>
    /// <returns></returns>
    public string HandToString(int playerNum)
    {
        int hand = CheckHand(players[playerNum].cards);
        if (hand == ROYAL_FLUSH)    return "Royal Flush";        
        if (hand == STRAIGHT_FLUSH) return "Straight Flush";
        if (hand == FOUR_OF_A_KIND) return "Four of a Kind";
        if (hand == FULL_HOUSE)     return "Full House";
        if (hand == FLUSH)          return "Flush";
        if (hand == STRAIGHT)       return "Straight";
        if (hand == THREE_OF_A_KIND) return "Three of a Kind";
        if (hand == TWO_PAIR)       return "Two Pair";
        if (hand == ONE_PAIR)       return "One Pair";
        if (hand == HIGH_CARD)      return "High Card";
        else return "";
    }

    //? done -- don't touch
    /// <summary>
    /// Returns one if h1 is better, two if h2 is better, tie if they are exactly tied, -1 if not 5 card hands.
    /// Precondition: h1 and h2 must be arrays of 5 cards!
    /// </summary>
    /// <param name="h1">First array of Cards to compare.</param>
    /// <param name="one">Number to return if h1 is the winner.</param>
    /// <param name="h2">Second array of Cards to compare</param>
    /// <param name="two">Number to return if h2 is the winner.</param>
    /// <param name="tie">Number to return if h1 and h2 are equal.</param>
    /// <returns></returns>
    private int CompareTwoHands(Card[] h1, int one, Card[] h2, int two, int tie)
    {
        // check number of cards is 5
        if (h1.Length != 5 || h2.Length != 5) return -1;

        Card[] temp1 = h1;
        Card[] temp2 = h2;

        int high1, high2;

        // get base values for each hand
        int v1 = CheckHand(h1);
        int v2 = CheckHand(h2);

        // if different types of hands, return best hand
        if (v1 != v2) return (v1 > v2 ? one : two);

        // for same type of hands:
        if (v1 == ROYAL_FLUSH) return tie;      // Royal Flush has no tie breaker
        if (v1 == STRAIGHT_FLUSH)   // check high card
        {
            // check high card
            high1 = GetHighCardValue(h1);
            high2 = GetHighCardValue(h2);
            // same high card
            if (high1 == high2) return tie;
            // different high card
            return (high1 > high2 ? one : two);
        }
        if (v1 == FOUR_OF_A_KIND)   // check four, then check high card
        {
            int four1, four2;
            // check rank of four of a kind
            temp1 = SortByRank(h1);
            temp2 = SortByRank(h2);
            // same four of a kind
            if (temp1[2].rank == temp2[2].rank)
            {
                // check high card
                high1 = GetHighCardValue(h1);
                high2 = GetHighCardValue(h2);
                // same high card
                if (high1 == high2) return tie;
                // different high card
                return (high1 > high2 ? one : two);
            }
            four1 = temp1[2].rank;
            four2 = temp2[2].rank;

            // convert Aces (rank 1) to 14
            if (four1 == 1) four1 = 14;
            if (four2 == 1) four2 = 14;

            // different four of a kind
            return (four1 > four2 ? one : two);
        }
        if (v1 == FULL_HOUSE)   // check trips, then check pairs
        {   // aaabb or aabbb
            // check rank of 3 of a kind
            temp1 = SortByRank(h1);
            temp2 = SortByRank(h2);
            // same rank of 3 of a kind
            if (temp1[2].rank == temp2[2].rank)
            {
                // check rank of pair
                int full1 = (temp1[1].rank == temp1[2].rank) ? temp1[3].rank : temp1[1].rank;
                int full2 = (temp2[1].rank == temp2[2].rank) ? temp2[3].rank : temp2[1].rank;
                // same pair
                if (full1 == full2) return tie;
                // convert Aces (rank 1) to 14
                if (full1 == 1) full1 = 14;
                if (full2 == 1) full2 = 14;
                // different pair
                return (full1 > full2 ? one : two);
            }
            int three1 = temp1[2].rank;
            int three2 = temp2[2].rank;
            // convert Aces (rank 1) to 14
            if (three1 == 1) three1 = 14;
            if (three2 == 1) three2 = 14;
            // different 3 of a kind
            return (three1 > three2 ? one : two);
        }
        if (v1 == FLUSH)    // check high card through all 5 cards
        {
            // convert Aces (rank 1) to 14
            temp1 = SetAcesHigh(h1);
            temp2 = SetAcesHigh(h2);
            // sort
            temp1 = SortByRank(temp1);
            temp2 = SortByRank(temp2);

            // check high card
            if (temp1[4].rank == temp2[4].rank)
                {
                    // same next high card
                    if (temp1[3].rank == temp2[3].rank)
                    {
                        // same next high card
                        if (temp1[2].rank == temp2[2].rank)
                        {
                            // same next high card
                            if (temp1[1].rank == temp2[1].rank)
                            {
                                // same next high card
                                if (temp1[0].rank == temp2[0].rank) return tie;
                                return temp1[0].rank > temp2[0].rank ? one : two;
                            }
                            return temp1[1].rank > temp2[1].rank ? one : two;
                        }
                        return temp1[2].rank > temp2[2].rank ? one : two;
                    }
                    return temp1[3].rank > temp2[3].rank ? one : two;
                }
                return temp1[4].rank > temp2[4].rank ? one : two;
        }
        if (v1 == STRAIGHT) // check high card
        {
            // check high card
            high1 = GetHighCardValue(h1);
            high2 = GetHighCardValue(h2);
            // same high card
            if (high1 == high2) return tie;
            // different high card
            return (high1 > high2 ? one : two);
        }
        if (v1 == THREE_OF_A_KIND)
        {
            temp1 = SortByRank(h1);
            temp2 = SortByRank(h2);

            return CompareThreeOfAKind(temp1, one, temp2, two, tie);
        }
        if (v1 == TWO_PAIR)
        {
            temp1 = SortByRank(h1);
            temp2 = SortByRank(h2);

            return CompareTwoPair(temp1, one, temp2, two, tie);
        }
        if (v1 == ONE_PAIR)
        {
            temp1 = SortByRank(h1);
            temp2 = SortByRank(h2);

            return CompareOnePair(temp1, one, temp2, two, tie);
        }

        // high card
        // convert Aces (rank 1) to 14
        temp1 = SetAcesHigh(h1);
        temp2 = SetAcesHigh(h2);
        // sort
        temp1 = SortByRank(temp1);
        temp2 = SortByRank(temp2);

        // check high card
        if (temp1[4].rank == temp2[4].rank)
        {
            // same next high card
            if (temp1[3].rank == temp2[3].rank)
            {
                // same next high card
                if (temp1[2].rank == temp2[2].rank)
                {
                    // same next high card
                    if (temp1[1].rank == temp2[1].rank)
                    {
                        // same next high card
                        if (temp1[0].rank == temp2[0].rank) return tie;
                        return temp1[0].rank > temp2[0].rank ? one : two;
                    }
                    return temp1[1].rank > temp2[1].rank ? one : two;
                }
                return temp1[2].rank > temp2[2].rank ? one : two;
            }
            return temp1[3].rank > temp2[3].rank ? one : two;
        }
        return temp1[4].rank > temp2[4].rank ? one : two;
    }

    //? done -- don't touch
    #region Private Helper Methods

    /* Methods to determine the hand
     * 1. Royal Flush       : 10, J, Q, K, A of same suit
     * 2. Straight Flush    : 5 card straight of same suit
     * 3. Four of a Kind    : 4 cards of same rank
     * 4. Full House        : 3 of a kind and a pair
     * 5. Flush             : 5 cards of same suit
     * 6. Straight          : 5 cards in sequence
     * 7. Three of a kind   : 3 cards of same rank
     * 8. Two Pair          : 2 different pairs
     * 9. Pair              : 2 cards of same rank
     * 10.High card         : none of above, use highest rank card 
     */

    /// <summary>
    /// Returns base value of hand (-1 is invalid).
    /// Precondition: h1 and h2 must be arrays of 5 cards!
    /// </summary>
    /// <param name="cards">Array of 5 Cards.</param>
    /// <returns>Value of hand (-1 is invalid)</returns>
    private int CheckHand(Card[] cards)
    {
        // check number of cards is 5
        if (cards.Length != 5) return -1;

        // sort cards by suit
        Card[] myCards = SortBySuit(cards);

        // Check for flushes
        // check for all same suit
        if (myCards[0].suit == myCards[4].suit)
        {
            // sort by rank
            myCards = SortByRank(myCards);
            // check straight: 10, J, Q, K, A
            if (myCards[0].rank == 1 && myCards[1].rank == 10 && myCards[2].rank == 11
                && myCards[3].rank == 12 && myCards[4].rank == 13)
            {
                // Royal Flush!
                return ROYAL_FLUSH;
            }
            // check other straights
            if (CheckStraight(myCards))
            {
                // Straight Flush!
                return STRAIGHT_FLUSH;
            }
            // Flush!
            return FLUSH;
        }

        // sort by rank
        myCards = SortByRank(myCards);

        // check for Four of a Kind
        if (((myCards[0].rank == myCards[1].rank) && (myCards[1].rank == myCards[2].rank) && (myCards[2].rank == myCards[3].rank))
            || ((myCards[1].rank == myCards[2].rank) && (myCards[2].rank == myCards[3].rank) && (myCards[3].rank == myCards[4].rank)))
        {
            // Four of a Kind!
            return FOUR_OF_A_KIND;
        }

        // check for Full House
        if (((myCards[0].rank == myCards[1].rank) && (myCards[1].rank == myCards[2].rank) && (myCards[3].rank == myCards[4].rank))
            || ((myCards[0].rank == myCards[1].rank) && (myCards[2].rank == myCards[3].rank) && (myCards[3].rank == myCards[4].rank)))
        {
            // Full House!
            return FULL_HOUSE;
        }

        // check for Straight
        if (CheckStraight(myCards))
        {
            // Straight!
            return STRAIGHT;
        }

        // check for Three of a Kind
        if (((myCards[0].rank == myCards[1].rank) && (myCards[1].rank == myCards[2].rank))
            || ((myCards[1].rank == myCards[2].rank) && (myCards[2].rank == myCards[3].rank))
            || ((myCards[2].rank == myCards[3].rank) && (myCards[3].rank == myCards[4].rank)))
        {
            // Three of a Kind!
            return THREE_OF_A_KIND;
        }

        // check for Two Pair  
        if (((myCards[0].rank == myCards[1].rank) && (myCards[2].rank == myCards[3].rank))
           || ((myCards[0].rank == myCards[1].rank) && (myCards[3].rank == myCards[4].rank))
           || ((myCards[1].rank == myCards[2].rank) && (myCards[3].rank == myCards[4].rank)))
        {
            // Two Pair!
            return TWO_PAIR;
        }

        // check for Pair
        if ((myCards[0].rank == myCards[1].rank) || (myCards[1].rank == myCards[2].rank)
            || (myCards[2].rank == myCards[3].rank) || (myCards[3].rank == myCards[4].rank))
        {
            // Pair!
            return ONE_PAIR;
        }

        // High card!
        return HIGH_CARD;
    }

    /// <summary>
    /// Helper to CheckHand() and CompareHands(). Returns the value for the highest card in the given hand.
    /// </summary>
    /// <param name="cards"></param>
    /// <returns></returns>
    private int GetHighCardValue(Card[] cards)
    {
        // sort by rank
        Card[] myCards = SortByRank(cards);

        // return 14 for Ace
        if (myCards[0].rank == 1) return 14;
        // return rank value
        else return myCards[myCards.Length - 1].rank;
    }

    /// <summary>
    /// Helper to CheckHand(). Checks if a 5-Card hand is a Straight.
    /// Precondition: h1 and h2 must be arrays of 5 cards!
    /// </summary>
    /// <param name="cards">Array of 5 Cards.</param>
    /// <returns>True if the hand is a Straight.</returns>
    private bool CheckStraight(Card[] cards)
    {
        // check number of cards is 5
        if (cards.Length != 5) return false;

        // sort by rank
        Card[] myCards = SortByRank(cards);

        // check for hands with Ace (can be used as 1 or 14)
        if (myCards[0].rank == 1)
        {
            return ((myCards[1].rank == 2 && myCards[2].rank == 3 && myCards[3].rank == 4 && myCards[4].rank == 5)
                || (myCards[1].rank == 10 && myCards[2].rank == 11 && myCards[3].rank == 12 && myCards[4].rank == 13));
        }

        // check for hands without Ace
        // get rank of first card
        int myRank = myCards[0].rank;

        // check each next card rank is +1 previous card rank
        for (int i = 1; i < 5; i++)
        {
            if (myCards[i].rank != myRank++)
                return false;
        }

        return true;
    }

    /// <summary>
    /// Helper to CompareHands(). Determines the winner(s) of 3-of-a-kind hands.
    /// Precondition: h1 and h2 must be arrays of 5 cards!
    /// </summary>
    /// <param name="h1">First array of Cards to compare.</param>
    /// <param name="one">Number to return if h1 is the winner.</param>
    /// <param name="h2">Second array of Cards to compare</param>
    /// <param name="two">Number to return if h2 is the winner.</param>
    /// <param name="tie">Number to return if h1 and h2 are equal.</param>
    /// <returns></returns>
    private int CompareThreeOfAKind(Card[] h1, int one, Card[] h2, int two, int tie)
    {
        // sort by rank

        int threesRank1 = h1[2].rank;
        int threesRank2 = h2[2].rank;

        // convert Aces (rank 1) to 14
        if (threesRank1 == 1) threesRank1 = 14;
        if (threesRank2 == 1) threesRank2 = 14;

        // check for different three of a kind
        if (threesRank1 != threesRank2)
            return threesRank1 > threesRank2 ? one : two;

        int highCard1, lowCard1, highCard2, lowCard2;

        /// h1
        // xxxyz : 0, 1, 2
        if (h1[0].rank == h1[1].rank && h1[1].rank == h1[2].rank)
        {
            highCard1 = h1[4].rank;
            lowCard1 = h1[3].rank;
        }
        // xyyyz : 1, 2, 3
        else if (h1[1].rank == h1[2].rank && h1[2].rank == h1[3].rank)
        {
            highCard1 = h1[4].rank;
            lowCard1 = h1[0].rank;
        }
        // xyzzz : 2, 3, 4
        else
        {
            highCard1 = h1[1].rank;
            lowCard1 = h1[0].rank;
        }

        /// h2
        // xxxyz : 0, 1, 2
        if (h2[0].rank == h2[1].rank && h2[1].rank == h2[2].rank)
        {
            highCard2 = h2[4].rank;
            lowCard2 = h2[3].rank;
        }
        // xyyyz : 1, 2, 3
        else if (h2[1].rank == h2[2].rank && h2[2].rank == h2[3].rank)
        {
            highCard2 = h2[4].rank;
            lowCard2 = h2[0].rank;
        }
        // xyzzz : 2, 3, 4
        else
        {
            highCard2 = h2[1].rank;
            lowCard2 = h2[0].rank;
        }

        // convert Aces (rank 1) to 14
        if (highCard1 == 1) highCard1 = 14;
        if (lowCard1 == 1) lowCard1 = 14;
        if (highCard2 == 1) highCard2 = 14;
        if (lowCard2 == 1) lowCard2 = 14;

        // check high card
        if (highCard1 != highCard2)
            return highCard1 > highCard2 ? one : two;

        // check low card
        if (lowCard1 != lowCard2)
            return lowCard1 > lowCard2 ? one : two;

        return tie;
    }

    /// <summary>
    /// Helper to CompareHands(). Determines the winner(s) of 2-pair hands.
    /// Precondition: h1 and h2 must be arrays of 5 cards!
    /// </summary>
    /// <param name="h1">First array of Cards to compare.</param>
    /// <param name="one">Number to return if h1 is the winner.</param>
    /// <param name="h2">Second array of Cards to compare</param>
    /// <param name="two">Number to return if h2 is the winner.</param>
    /// <param name="tie">Number to return if h1 and h2 are equal.</param>
    /// <returns></returns>
    private int CompareTwoPair(Card[] h1, int one, Card[] h2, int two, int tie)
    {
        // check 5 card hands
        // sort by rank

        int firstRank1, firstRank2, secondRank1, secondRank2, fifthCard1, fifthCard2;

        /// h1
        // aaccb: 0&1, 2&3, 4
        if (h1[0].rank == h1[1].rank && h1[2].rank == h1[3].rank)
        {
            firstRank1 = h1[0].rank;
            secondRank1 = h1[2].rank;
            fifthCard1 = h1[4].rank;
        }
        // aabcc: 0&1, 2, 3&4
        else if (h1[0].rank == h1[1].rank && h1[3].rank == h1[4].rank)
        {
            firstRank1 = h1[0].rank;
            secondRank1 = h1[4].rank;
            fifthCard1 = h1[2].rank;
        }
        // baacc: 0, 1&2, 3&4
        else
        {
            firstRank1 = h1[1].rank;
            secondRank1 = h1[3].rank;
            fifthCard1 = h1[0].rank;
        }

        /// h2
        // aaccb: 0&1, 2&3, 4
        if (h2[0].rank == h2[1].rank && h2[2].rank == h2[3].rank)
        {
            firstRank2 = h2[0].rank;
            secondRank2 = h2[2].rank;
            fifthCard2 = h2[4].rank;
        }
        // aabcc: 0&1, 2, 3&4
        else if (h2[0].rank == h2[1].rank && h2[3].rank == h2[4].rank)
        {
            firstRank2 = h2[0].rank;
            secondRank2 = h2[4].rank;
            fifthCard2 = h2[2].rank;
        }
        // baacc: 0, 1&2, 3&4
        else
        {
            firstRank2 = h2[1].rank;
            secondRank2 = h2[3].rank;
            fifthCard2 = h2[0].rank;
        }

        // convert Aces (rank 1) to 14
        if (firstRank1 == 1) firstRank1 = 14;
        if (firstRank2 == 1) firstRank2 = 14;
        if (secondRank1 == 1) secondRank1 = 14;
        if (secondRank2 == 1) secondRank2 = 14;
        if (fifthCard1 == 1) fifthCard1 = 14;
        if (fifthCard2 == 1) fifthCard2 = 14;

        if (firstRank1 == firstRank2)
        {
            if (secondRank1 == secondRank2)
            {
                if (fifthCard1 == fifthCard2) return tie;
                return fifthCard1 > fifthCard2 ? one : two;
            }
            return secondRank1 > secondRank2 ? one : two;
        }
        return firstRank1 > firstRank2 ? one : two;
    }

    /// <summary>
    /// Helper to CompareHands(). Determines the winner(s) of 1-pair hands.
    /// Precondition: h1 and h2 must be arrays of 5 cards!
    /// </summary>
    /// <param name="h1">First array of Cards to compare.</param>
    /// <param name="one">Number to return if h1 is the winner.</param>
    /// <param name="h2">Second array of Cards to compare</param>
    /// <param name="two">Number to return if h2 is the winner.</param>
    /// <param name="tie">Number to return if h1 and h2 are equal.</param>
    /// <returns></returns>
    private int CompareOnePair(Card[] h1, int one, Card[] h2, int two, int tie)
    {
        // check 5 card hands
        // sort by rank

        int pairRank1, pairRank2, high1, high2, med1, med2, low1, low2;

        /// h1
        // abcdd
        if (h1[3].rank == h1[4].rank)
        {
            pairRank1 = h1[3].rank;
            high1 = h1[2].rank;
            med1 = h1[1].rank;
            low1 = h1[0].rank;
        }
        // abccd
        else if (h1[3].rank == h1[2].rank)
        {
            pairRank1 = h1[3].rank;
            high1 = h1[4].rank;
            med1 = h1[1].rank;
            low1 = h1[0].rank;
        }
        // abbcd
        else if (h1[1].rank == h1[2].rank)
        {
            pairRank1 = h1[2].rank;
            high1 = h1[4].rank;
            med1 = h1[3].rank;
            low1 = h1[0].rank;
        }
        // aabcd
        else
        {
            pairRank1 = h1[0].rank;
            high1 = h1[4].rank;
            med1 = h1[3].rank;
            low1 = h1[2].rank;
        }

        /// h2
        // abcdd
        if (h2[3].rank == h2[4].rank)
        {
            pairRank2 = h2[3].rank;
            high2 = h2[2].rank;
            med2 = h2[1].rank;
            low2 = h2[0].rank;
        }
        // abccd
        else if (h2[3].rank == h2[2].rank)
        {
            pairRank2 = h2[3].rank;
            high2 = h2[4].rank;
            med2 = h2[1].rank;
            low2 = h2[0].rank;
        }
        // abbcd
        else if (h2[1].rank == h2[2].rank)
        {
            pairRank2 = h2[2].rank;
            high2 = h2[4].rank;
            med2 = h2[3].rank;
            low2 = h2[0].rank;
        }
        // aabcd
        else
        {
            pairRank2 = h2[0].rank;
            high2 = h2[4].rank;
            med2 = h2[3].rank;
            low2 = h2[2].rank;
        }

        // convert Aces (rank 1) to 14
        if (pairRank1 == 1) pairRank1 = 14;
        if (pairRank2 == 1) pairRank2 = 14;
        if (high1 == 1) high1 = 14;
        if (high2 == 1) high2 = 14;
        if (med1 == 1) med1 = 14;
        if (med2 == 1) med2 = 14;
        if (low1 == 1) low1 = 14;
        if (low2 == 1) low2 = 14;

        if (pairRank1 == pairRank2)
        {
            if (high1 == high2)
            {
                if (med1 == med2)
                {
                    if (low1 == low2) return tie;
                    return low1 > low2 ? one : two;
                }
                return med1 > med2 ? one : two;
            }
            return high1 > high2 ? one : two;
        }
        return pairRank1 > pairRank2 ? one : two;
    }

    /// <summary>
    /// Sorts an array of Cards by rank.
    /// </summary>
    /// <param name="cards">The array of Cards to sort.</param>
    /// <returns>An array of rank-sorted cards.</returns>
    private Card[] SortByRank(Card[] cards)
    {
        Card[] temp = cards;

        int min, i, j;

        // check each card to sort the hand
        for (i = 0; i < temp.Length; i++)
        {
            // set min to first card
            min = i;

            // check every card after first card
            for (j = i + 1; j < temp.Length; j++)
            {
                if (temp[j].rank < temp[min].rank)
                    min = j;
            }
            // swap i with min
            Card c = temp[i];
            temp[i] = temp[min];
            temp[min] = c;
        }
        return temp;
    }

    /// <summary>
    /// Changes the rank of Aces from 1 to 14.
    /// </summary>
    /// <param name="cards"></param>
    /// <returns></returns>
    private Card[] SetAcesHigh(Card[] cards)
    {
        Card[] temp = cards;

        // check each card
        for (int i = 0; i < temp.Length; i++)
        {
            if (temp[i].rank == 1) temp[i].rank = 14;
        }
        return temp;
    }

    /// <summary>
    /// Sorts an array of Cards by suit.
    /// </summary>
    /// <param name="cards">The array of Cards to sort.</param>
    /// <returns>An array of suit-sorted cards.</returns>
    private Card[] SortBySuit(Card[] cards)
    {
        Card[] temp = cards;

        int min, i, j;

        // check each card to sort the hand
        for (i = 0; i < temp.Length; i++)
        {
            // set min to first card
            min = i;

            // check every card after first card
            for (j = i + 1; j < temp.Length; j++)
            {
                if (temp[j].suit < temp[min].suit)
                    min = j;
            }
            // swap i with min
            Card c = temp[i];
            temp[i] = temp[min];
            temp[min] = c;
        }
        return temp;
    }

    #endregion

    #endregion

    //? add more types of requests here!
    /// <summary>
    /// Processes a client request and performs actions on the server.
    /// </summary>
    /// <param name="input">String data received from a client.</param>
    /// <returns>String response to send to client.</returns>
    private string ProcessRequest(string clientInfo, string method, string body)
    {
        string returnMethod = ActionsLibrary.EMPTY_STRING;
        string returnClient = ActionsLibrary.EMPTY_STRING;
        string returnBody = ActionsLibrary.EMPTY_STRING;

        if (method.CompareTo(ActionsLibrary.GET_GAME_INFO) == 0)
        {
            // game name, owner, num human players, player names
            returnMethod = ActionsLibrary.RETURN_GAME_INFO;
            returnBody = ActionsLibrary.GameInfoToString(gameName, playerNames[0], numHumanPlayers, playerNames);
        }

        //? add more types of requests here!

        return ActionsLibrary.FormMessage(returnClient, returnMethod, returnBody);
    }

    /// <summary>
    /// Called by the server network manager when receiving a request from a client.
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public string ProcessRequest(string input)
    {
        string[] stringParts = ActionsLibrary.SplitMessage(input);
        return ProcessRequest(stringParts[0], stringParts[1], stringParts[2]);
    }
}
