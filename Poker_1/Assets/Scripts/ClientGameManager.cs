﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientGameManager : MonoBehaviour
{
    // references to the other client scripts in this scene
    private ClientNetworkManager clientNetMgr;
    private ClientUIController clientUICtrl;

    #region Player Data
    // this player's id number
    [Range(1, ServerGameManager.MAX_PLAYERS)]
    private int playerNum;          // 1 to 4 player number   
    private Card[] cards;           // this player's hand of cards    
    private bool myTurn = false;    // true if it this player's turn in gameplay
    #endregion

    #region Game Data
    private string gameName;
    private ServerGameManager.GameStatus status;
    private string[] playerNames;       // the player name (string)
    private bool[] humanPlayers;        // true if human player, false if AI
    private int numHumanPlayers;        // total number of human players in this game
    private bool[] playersReady;        // true if player readied up, true for AIs always
    #endregion

    #region Game Process
    private Queue<string> ActionsQueue;     // queue of actions to perform on the client (in order)
    #endregion

    private void Start()
    {
        clientNetMgr = FindObjectOfType<ClientNetworkManager>();
        clientUICtrl = FindObjectOfType<ClientUIController>();

        playerNames = new string[ServerGameManager.MAX_PLAYERS];
        humanPlayers = new bool[ServerGameManager.MAX_PLAYERS];
        playersReady = new bool[ServerGameManager.MAX_PLAYERS];
        ActionsQueue = new Queue<string>();
    }

    //? add more types of requests here!
    public void SendRequest(string method)
    {
        string client = ActionsLibrary.EMPTY_STRING;
        string body = ActionsLibrary.EMPTY_STRING;

        // check method is not empty
        if (string.IsNullOrEmpty(method)) return;

        // set client info
        client = (clientNetMgr.myIP + " " + playerNum.ToString());

        // set body
        if (method.CompareTo(ActionsLibrary.GET_GAME_INFO) == 0)
        {
           // don't need body for this request
        }

        //? add more types of requests here!

        // format and send the request
        clientNetMgr.SendRequest(ActionsLibrary.FormMessage(client, method, body));
    }


    //? add more types of actions here!
    /// <summary>
    /// Perform some action on the client application based on response from the server.
    /// </summary>
    /// <param name="clientInfo"></param>
    /// <param name="method"></param>
    /// <param name="body"></param>
    private void ProcessResponse(string clientInfo, string method, string body)
    {
        //? add more types of actions here!
        if (method.CompareTo(ActionsLibrary.RETURN_GAME_INFO) == 0)
        {
            // display game info

            // temporarily save game info? save list of games?
        }
    }

    /// <summary>
    /// Called by the client network manager to process a response from the server.
    /// </summary>
    /// <param name="response"></param>
    public void ProcessResponse(string response)
    {
        //? Do some action on the client application based on the data received
        string[] stringParts = ActionsLibrary.SplitMessage(response);
        ProcessResponse(stringParts[0], stringParts[1], stringParts[2]);
    }

    #region Set Game Variables
    public void SetGameName(string name) { gameName = name; }
    public void SetGameStatus(ServerGameManager.GameStatus s) { status = s; }
    public void SetPlayerName(int num, string name) { playerNames[num] = name; }
    public void SetHumanPlayer(int num, bool b) { humanPlayers[num] = b; }
    public void SetNumHumanPlayers(int num) { numHumanPlayers = num; }
    public void SetPlayerReady(int num, bool b) { playersReady[num] = b; }
    #endregion

    #region Set Player Variables
    /// <summary>
    /// Set the player's number
    /// </summary>
    /// <param name="n"></param>
    public void SetNumber(int n)
    {
        playerNum = n;
    }

    /// <summary>
    /// Set this player's turn when the can do some action in the game.
    /// </summary>
    /// <param name="b">True if this player can do action in the game.</param>
    public void SetTurn(bool b)
    {
        myTurn = b;
    }

    /// <summary>
    /// Set the players hand of 5 cards.
    /// </summary>
    /// <param name="c">Array of 5 cards.</param>
    /// <returns>False if c is more or less than 5 cards.</returns>
    public bool SetCards(Card[] c)
    {
        if (c.Length != ServerGameManager.HAND_SIZE) return false;
        cards = c;
        return true;
    }
    #endregion
}
