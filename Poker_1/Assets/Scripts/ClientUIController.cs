﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClientUIController : MonoBehaviour
{
    // references to the other client scripts in this scene
    private ClientNetworkManager clientNetMgr;
    private ClientGameManager clientGameMgr;

    #region List Games
    public GameObject listGamesCanvas;
    [SerializeField]
    private Button joinGameButton;
    public UIListEntry firstEntry;

    private void ShowJoinGame()
    {
        joinGameCanvas.SetActive(true);
    }

    #endregion

    #region Join Game
    public GameObject joinGameCanvas;
    [SerializeField]
    private Text gameName_join;
    [SerializeField]
    private Text gameOwner_join;
    [SerializeField]
    private GameObject[] players_join;
    
    [SerializeField]
    private InputField gameNameInput;
    [SerializeField]
    private InputField passwordInput;
    #endregion


    // Start is called before the first frame update
    void Start()
    {
        clientNetMgr = FindObjectOfType<ClientNetworkManager>();
        clientGameMgr = FindObjectOfType<ClientGameManager>();

        // set buttons
        joinGameButton.onClick.AddListener(ShowJoinGame);

        // contact server to get game info
        clientGameMgr.SendRequest(ActionsLibrary.GET_GAME_INFO);
    }

    public void DisplayGameInfo(string gameName, string owner, int numPlayers, string[] names)
    {
        if (listGamesCanvas.activeInHierarchy)
        {
            firstEntry.SetName(gameName);
            firstEntry.SetOwner(owner);
            for (int i = 0; i < 5; i++)
            {
                if (i < numPlayers)
                    firstEntry.PlayerIn(i, true);
                else firstEntry.PlayerIn(i, false);
            }
        }

        if (joinGameCanvas.activeInHierarchy)
        {

        }
    }
    
}
