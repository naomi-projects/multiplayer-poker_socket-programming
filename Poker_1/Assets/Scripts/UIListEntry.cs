﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIListEntry : MonoBehaviour
{
    public Text gamename;
    public Text gameOwner;
    public Image[] playersIn;
    public Color playerInColor;

    public void PlayerIn(int number, bool isReady)
    {
        if (number >= playersIn.Length) return;

        if (isReady) playersIn[number].color = playerInColor;
        else playersIn[number].color = Color.white;
    }

    public void SetName(string name)
    {
        gamename.text = name;
    }

    public void SetOwner(string name)
    {
        gameOwner.text = name;
    }
}
