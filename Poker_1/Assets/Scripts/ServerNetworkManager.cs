﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Text;
using UnityEngine.UI;

#pragma warning disable 0649

/// <summary>
/// Manages the server network communication
/// </summary>
public class ServerNetworkManager : MonoBehaviour
{
    // reference to the other server scripts in this scene
    //private ServerUIController serverUICtrl;
    //private ServerGameManager serverGameMgr;

    // used to pass socket and buffer data to delegate
    internal class StateObject
    {
        internal byte[] buffer;
        internal Socket socket;
        internal StateObject(int size, Socket sock)
        {
            buffer = new byte[size];
            socket = sock;
        }
    }
   
    #region Network Variables
    
    // server system name
    private string serverName;
    
    // server system IP address
    private IPAddress serverIP;
    
    // socket to listen for new connections
    private Socket mySocket;
    
    // port number
    private const int port = 2001;
    
    // maximums bytes to send between client and server
    private const int MAX_BYTES = 1024; 
    
    // if server currently 
    private bool acceptingNewConnections = false;


    #endregion
    
    // for developers... 
    // Set to true to show log messages in the console using Display()
    public bool showDebugLog = true;

    // Start
    private void Start()
    {
        //serverUICtrl = FindObjectOfType<ServerUIController>();
        //serverGameMgr = FindObjectOfType<ServerGameManager>();
    }

    /// <summary>
    /// Stops the server connection. 
    /// </summary>
    public void StopServer()
    {
        // stop checking for client requests
        CancelInvoke();
        acceptingNewConnections = false;
        Display("Stopping the server...");
        //mySocket.Disconnect(false);
        //mySocket.Shutdown(SocketShutdown.Both);
        mySocket.Close();
    }

    /// <summary>
    /// Starts the network setup on the server side.
    /// </summary>
    public void StartServer()
    {
        // get server name
        serverName = Dns.GetHostName();
        Display("serverName: " + serverName);

        // get server IP
        serverIP = Dns.GetHostEntry(serverName).AddressList[0];
        Display("serverIP: " + serverIP);
        Display("serverIP.AddressFamily: " + serverIP.AddressFamily.ToString());

        // make network endpoint
        IPEndPoint serverEndPoint = new IPEndPoint(serverIP, port);

        // make new socket
        mySocket = new Socket(serverIP.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        Display("Server socket created.");

        // bind the socket to this network endpoint
        mySocket.Bind(serverEndPoint);

        // put server socket in listening state
        // 10: max length of pending connections queue
        mySocket.Listen(10);
        Display("Server socket is now listening...");

        acceptingNewConnections = true;

        // start listening for requests sent by clients (check for incoming data every 5 seconds)
        //InvokeRepeating("CheckForConnectionRequest", 0, 5);
    }

    /// <summary>
    /// Accepts new connecting clients while the server is listening.
    /// Preconditions: The Server is set up for communication and listening for new connections via StartServer().
    /// </summary>
    public void CheckForConnectionRequest()
    {
        if (!acceptingNewConnections)
        {
            Display("Server not accepting new connections.");
            return;
        }
        // begin asynchronous operation to accept an incoming connection attempt
        IAsyncResult asyncAccept = mySocket.BeginAccept(new AsyncCallback(ServerNetworkManager.AcceptCallback), mySocket);
        Display("Checking for incoming requests...");
    }

    /// <summary>
    /// This callback will be executed if a client tries to connect to the server.
    /// </summary>
    /// <param name="asyncAccept"></param>
    private static void AcceptCallback(IAsyncResult asyncAccept)
    {
        Socket listenSocket = (Socket)asyncAccept.AsyncState;

        // create a new socket for the connection
        Socket serverSocket = listenSocket.EndAccept(asyncAccept);

        if (serverSocket.Connected == false)
        {
            Debug.Log("Server is not connected.");
            return;
        }

        Debug.Log("Server is connected.");

        listenSocket.Close();

        // create object to hold info for the serverSocket and data buffer
        StateObject stateObject = new StateObject(MAX_BYTES, serverSocket);

        // begin asynchronous receive data from the connected client
        // store the data in stateObject.buffer, get maximum bytes to fill the buffer
        // invokes ReceiveCallback when the operation is complete
        IAsyncResult asyncReceive = serverSocket.BeginReceive(
            stateObject.buffer, 
            0, 
            stateObject.buffer.Length,
            SocketFlags.None, 
            new AsyncCallback(ReceiveCallback), 
            stateObject         // StateObject passed to ReceiveCallback
            );
        Debug.Log("Receiving data...");
        // Timeout(asyncReceive);
    }

    /// <summary>
    /// This callback will be executed if a connected client tries to send data to the server.
    /// </summary>
    /// <param name="asyncReceive"></param>
    private static void ReceiveCallback(IAsyncResult asyncReceive)
    {
        // get the socket and data buffer
        StateObject stateObject = (StateObject)asyncReceive.AsyncState;

        // end the pending asynchronous read and get the number of bytes received
        int bytesReceived = stateObject.socket.EndReceive(asyncReceive);

        // get the string sent by the client
        string input = Encoding.ASCII.GetString(stateObject.buffer);
        Debug.Log("Data received: " + input);

        //? this is when actions are performed on the server application (ServerGameManager::DoAction)
        // do action on the server based on the received data
        // and get a response string to send back to the client
        ServerGameManager gameMgr = FindObjectOfType<ServerGameManager>();
        string response = gameMgr.ProcessRequest(input);
              
        // change the string to bytes
        byte[] sendData = Encoding.ASCII.GetBytes(response);

        // asynchronous send the response to the client
        IAsyncResult asyncSend = stateObject.socket.BeginSend(
            sendData,
            0,
            response.Length,
            SocketFlags.None,
            new AsyncCallback(SendCallback),
            stateObject.socket      // Socket passed to SendCallback
            );
        Debug.Log("Sending data: " + sendData);
        // Timeout(asyncSend);
    }

    /// <summary>
    /// This callback will be executed if the server tries to send data to a client.
    /// </summary>
    /// <param name="asyncSend"></param>
    private static void SendCallback(IAsyncResult asyncSend)
    {
        // get the socket 
        Socket serverSocket = (Socket)asyncSend.AsyncState;
        
        // end the pending asynchronous send, get number of bytes sent
        int bytesSent = serverSocket.EndSend(asyncSend);
        Debug.Log("Bytes sent: " + bytesSent.ToString());

        // close the connection
        serverSocket.Shutdown(SocketShutdown.Both);
        serverSocket.Close();
    }
    
    // time out after 20 seconds
    internal static bool Timeout(IAsyncResult ar)
    {
        int i = 0;
        while (ar.IsCompleted == false)
        {
            if (i++ > 40)
            {
                Debug.Log("Timed out.");
                return false;
            }
            // check every 500 milliseconds
            Thread.Sleep(500);
        }
        return true;
    }

    // display string on console and on UI display
    private void Display(string s)
    {
        if (showDebugLog)
            Debug.Log(s);
    }
}
