﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentralServer : MonoBehaviour
{
    private const int GAME_ID = 1111;

    public class GameInfo
    {
        public int gameID;

        public string gameName;
        public string[] playerNames; 
        public int numHumanPlayers = 1;

        public string password;
    }

    private List<GameInfo> gameList;

    private void Start()
    {
        gameList = new List<GameInfo>();
    }

    public bool AddPlayer(string name, int id)
    {
        int index = GetIndex(id);
        // game with the given ID number does not exist
        if (index == -1) return false;
        // game has maximum players
        if (gameList[index].numHumanPlayers >= ServerGameManager.MAX_PLAYERS) return false;
        // add the player
        gameList[index].numHumanPlayers++;
        gameList[index].playerNames[gameList[index].numHumanPlayers - 1] = name;
        return true;
    }

    public bool RemovePlayer(string name, int id)
    {
        int index = GetIndex(id);
        // game with the given ID number does not exist
        if (index == -1) return false;
        // game has no players, and cannot remove game owner
        if (gameList[index].numHumanPlayers <= 1) return false;
        // find the player in the list, cannot remove the game owner
        for (int i = 1; i < gameList[index].playerNames.Length; i++)
        {
            if (gameList[index].playerNames[i].CompareTo(name) == 0)
            {
                // remove the player
                gameList[index].numHumanPlayers--;
                gameList[index].playerNames[i] = null;
                if (i == gameList[index].numHumanPlayers) return true;
                string lastPlayerName = gameList[index].playerNames[gameList[index].numHumanPlayers];
                gameList[index].playerNames[i] = lastPlayerName;
                return true;
            }
        }
        return false;
    }

    private int GetIndex(int id)
    {
        int index = -1;
        for (int i = 0; i < gameList.Count; i++)
        {
            if (gameList[i].gameID == id)
            {
                index = i;
                break;
            }
        }
        return index;
    }

    /// <summary>
    /// Add a new game to the list.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="owner"></param>
    /// <param name="pass"></param>
    /// <returns></returns>
    public int AddGame(string name, string owner, string pass)
    {
        int id = GAME_ID + gameList.Count;
        gameList.Add(new GameInfo() {
            gameID = id,
            gameName = name,
            playerNames = new string[ServerGameManager.MAX_PLAYERS] { owner, null, null, null, null },
            numHumanPlayers = 1,
            password = pass
        });

        return id;
    }

    /// <summary>
    /// Remove a game from the list.
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public bool RemoveGame(int id)
    {
        for (int i = 0; i < gameList.Count; i++)
        {
            if (gameList[i].gameID == id)
            {
                gameList.RemoveAt(i);
                return true;
            }
        }
        return false;
    }

    public string ListToString()
    {
        string s = "";
        
        for (int i = 0; i < gameList.Count; i++)
        {
            s += (GameToString(i) + ">]");
        }

        return s;
    }

    public List<GameInfo> StringToList(string s)
    {
        string[] separatingChars = { ">]" };
        string[] games = s.Split(separatingChars, StringSplitOptions.RemoveEmptyEntries);
        List<GameInfo> list = new List<GameInfo>();
        foreach (string str in games)
        {
            list.Add(StringToGame(str));
        }
        return list;
    }

    public string GameToString(int index)
    {
        string s = "";

        s += (gameList[index].gameID.ToString() + " ");
        s += (gameList[index].gameName + " ");
        s += (gameList[index].playerNames[0] + " ");
        s += (gameList[index].numHumanPlayers.ToString() + " ");
        for (int i = 0; i < gameList[index].numHumanPlayers; i++)
        {
            s += (gameList[index].playerNames[i] + " ");
        }
        s += (gameList[index].password + " ");
        // "1111 mygamename ownername 3 player1 player2 player3 pass "

        return s;
    }

    public GameInfo StringToGame(string s)
    {
        char[] charSeparators = new char[] { ' ' };
        string[] result = s.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);

        GameInfo gi = new GameInfo() {
            gameID = Int32.Parse(result[0]),
            gameName = result[1],
            playerNames = new string[ServerGameManager.MAX_PLAYERS] { result[2], null, null, null, null },
            numHumanPlayers = Int32.Parse(result[3]),
            password = result[result.Length - 1]
        };

        for (int i = 0; i < gi.numHumanPlayers; i++)
        {
            gi.playerNames[i + 1] = result[i + 4];
        }
        return gi;
    }
}
