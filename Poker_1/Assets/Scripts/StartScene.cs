﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Net;

public class StartScene : MonoBehaviour
{
    public Button startServerButton;
    public Button startClientButton;

    // Start is called before the first frame update
    void Start()
    {
        startServerButton.onClick.AddListener(StartServer);
        startClientButton.onClick.AddListener(StartClient);
    }

    private void StartServer()
    {
        SceneManager.LoadScene(1);
    }

    private void StartClient()
    {
        SceneManager.LoadScene(2);
    } 
}
