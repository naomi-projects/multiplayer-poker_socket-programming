﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

#pragma warning disable 0649

public class ServerUIController : MonoBehaviour
{
    // references to the other server scripts in this scene
    private ServerNetworkManager serverNetMgr;
    private ServerGameManager serverGameMgr;
    
    #region CreateGameCanvas Variables
    public GameObject createGameCanvas;
    [SerializeField]
    private Button createGameButton;
    [SerializeField]
    private InputField gameNameInput;
    [SerializeField]
    private InputField passwordInput;
    #endregion

    #region LobbyCanvas Variables
    public GameObject lobbyCanvas;
    [SerializeField]
    private GameObject startGameButton;     // server-client start button
    [SerializeField]
    private GameObject[] playerReadyIcons;  // player 2 through 5 ready icons
    [SerializeField]
    private Text[] playerNamesTexts;        // UI to show the player names
    #endregion

    #region GameplayCanvas Variables
    public GameObject gameplayCanvas;
    public GameObject[] cardSuits;
    public GameObject[] cardRanks;
    [SerializeField]
    private Button showAllButton;
    [SerializeField]
    private Button getWinnersButton;
    [SerializeField]
    private GameObject winnerTextGroup;
    [SerializeField]
    private Text winningPlayerText;
    [SerializeField]
    private Text winningHandText;
    [SerializeField]
    private Button gameDoneButton;
    [SerializeField]
    private Sprite club;
    [SerializeField]
    private Sprite spade;
    [SerializeField]
    private Sprite heart;
    [SerializeField]
    private Sprite diamond;

    #endregion

    #region CreateGame Functions
    /// <summary>
    /// Creates a new game lobby and starts the server.
    /// </summary>
    private void CreateGame()
    {
        if (string.IsNullOrWhiteSpace(gameNameInput.text))
        {
            // no game name entered
            Debug.LogError("No game name entered. Game not created.");
            return;
        }
        else if (string.IsNullOrWhiteSpace(passwordInput.text))
        {
            // no password entered
            Debug.LogError("No password entered. Game not created.");
            return;
        }

        // show the game lobby
        createGameCanvas.SetActive(false);
        lobbyCanvas.SetActive(true);
        // game status is now inlobby
        serverGameMgr.SetStatus(ServerGameManager.GameStatus.inlobby);
        // start the server
        serverNetMgr.StartServer();
        // set the game name and password
        serverGameMgr.CreateGame(gameNameInput.text, passwordInput.text);        
    }
    #endregion

    #region Lobby Functions

    /// <summary>
    /// Called when lobby leader starts the game.
    /// </summary>
    private void StartGame()
    {
        // check if lobby is inactive
        if (lobbyCanvas.activeInHierarchy == false) return;

        // switch the canvas from lobby to ingame
        lobbyCanvas.SetActive(false);
        gameplayCanvas.SetActive(true);        
        //? FindGameObjectsWithTag returns objects in hierarchical order??
        cardSuits = GameObject.FindGameObjectsWithTag("Suit");
        cardRanks = GameObject.FindGameObjectsWithTag("Rank");
        // start the game
        serverGameMgr.StartGame();
    }

    /// <summary>
    /// Set a player to ready or not ready. Called by ServerGameManager.
    /// </summary>
    /// <param name="ready"></param>
    /// <param name="number"></param>
    public bool SetLobbyUI_PlayerReady(bool ready, int number)
    {
        // check if lobby is inactive
        if (lobbyCanvas.activeInHierarchy == false) return false;
        
        playerReadyIcons[number].SetActive(ready);

        return true;
    }

    /// <summary>
    /// Set the UI for a player connected to the lobby. Called by ServerGameManager.
    /// </summary>
    /// <param name="name"></param>
    /// <param name="number">Must be 1 to 4.</param>
    public bool SetLobbyUI_PlayerName(string name, int number)
    {
        // check if lobby is inactive
        if (lobbyCanvas.activeInHierarchy == false) return false;
        
        // set the UI display
        playerNamesTexts[number].text = name;

        return true;
    }

    #endregion

    #region In-Game Functions

    /// <summary>
    /// Sets the card UI for the specified player card.
    /// </summary>
    /// <param name="playerNum">Player number 0 to 4.</param>
    /// <param name="cardNum">0 to 4 number order of the card.</param>
    /// <param name="card"></param>
    public void SetCardUI(int playerNum, int cardNum, Card card)
    {
        // get the index number for the arrays
        int number = cardNum + (playerNum * 5);

        // get the suit image and color for the text
        Vector4 color;
        if (card.suit == Card.CardSuit.Clubs)
        {
            cardSuits[number].GetComponent<Image>().sprite = club;
            color = Color.black;
        }
        else if (card.suit == Card.CardSuit.Spades)
        {
            cardSuits[number].GetComponent<Image>().sprite = spade;
            color = Color.black;
        }
        else if (card.suit == Card.CardSuit.Hearts)
        {
            cardSuits[number].GetComponent<Image>().sprite = heart;
            color = Color.red;
        }
        else
        {
            cardSuits[number].GetComponent<Image>().sprite = diamond;
            color = Color.red;
        }
        
        // get string representation of the rank
        string rank;
        if (card.rank == 1) rank = "A";
        else if (card.rank == 13) rank = "K";
        else if (card.rank == 12) rank = "Q";
        else if (card.rank == 11) rank = "J";
        else rank = card.rank.ToString();
        
        // set the rank text
        cardRanks[number].GetComponent<Text>().text = rank;
        // set the rank color
        cardRanks[number].GetComponent<Text>().color = color;
    }

    /// <summary>
    /// Removes the cover on all the players' cards.
    /// </summary>
    public void UnhideAllCards()
    {
        GameObject[] cardBacks = GameObject.FindGameObjectsWithTag("CardBack");
        foreach (GameObject cardBack in cardBacks)
        {
            cardBack.SetActive(false);
        }
    }

    /// <summary>
    /// Reveal all players' cards.
    /// </summary>
    public void ShowAllCards()
    {
        serverGameMgr.SetStatus(ServerGameManager.GameStatus.postgame);
        // reveal the cards
        serverGameMgr.ShowAllCards();
        UnhideAllCards();
        // hide the show all button and unhide the get winners button
        showAllButton.gameObject.SetActive(false);
        getWinnersButton.gameObject.SetActive(true);
    }
    
    /// <summary>
    /// Determines the winners of the round and displays the winners.
    /// </summary>
    public void ShowWinners()
    {
        string s = "";
        // hide the button
        getWinnersButton.gameObject.SetActive(false);
        // unhide the winner text
        winnerTextGroup.SetActive(true);
        // get the winning player number(s)
        List<int> winners = serverGameMgr.EndRound();
        // set the winning players text
        foreach (int number in winners)
        {
            s += ("Player " + (number + 1).ToString() + " ");
        }
        winningPlayerText.text = s;
        // set the winning hand text
        winningHandText.text = serverGameMgr.HandToString(winners[0]);
        // show the exit button
        gameDoneButton.gameObject.SetActive(true);
    }

    /// <summary>
    /// Stop the server, exit the game, go back to the main menu.
    /// </summary>
    public void ExitGame()
    {
        serverNetMgr.StopServer();
        serverGameMgr.SetStatus(ServerGameManager.GameStatus.off);
        SceneManager.LoadScene(0);
    }

    #endregion

    // Start
    void Start()
    {
        for (int i = 0; i < ServerGameManager.MAX_PLAYERS; i++)
        {
            SetLobbyUI_PlayerReady(true, i);
        }

        serverNetMgr = FindObjectOfType<ServerNetworkManager>();
        serverGameMgr = FindObjectOfType<ServerGameManager>();

        // set buttons
        createGameButton.onClick.AddListener(CreateGame);
        startGameButton.GetComponent<Button>().onClick.AddListener(StartGame);
        showAllButton.onClick.AddListener(ShowAllCards);
        getWinnersButton.onClick.AddListener(ShowWinners);
        gameDoneButton.onClick.AddListener(ExitGame);        
    }
}
