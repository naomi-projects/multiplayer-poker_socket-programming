﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles all message formatting and parsing.
/// </summary>
public class ActionsLibrary : MonoBehaviour
{
    public const string delimiter = ">]";

    // Message Format:
    // sendingClientInfo>]method>]data>]
    public static string FormMessage(string client, string method, string body)
    {
        if (String.IsNullOrWhiteSpace(client)) client = EMPTY_STRING;
        if (String.IsNullOrWhiteSpace(method)) method = EMPTY_STRING;
        if (String.IsNullOrWhiteSpace(body)) body = EMPTY_STRING;

        return client + delimiter + method + delimiter + body + delimiter;
    }
    public static string[] SplitMessage(string input)
    {
        string[] separatingChars = { delimiter };
        return input.Split(separatingChars, StringSplitOptions.RemoveEmptyEntries);
    }

    #region Methods

    // update the client's state with the server's state
    public const string UPDATE_CLIENT = "update";

    // player fold
    public const string PLAYER_FOLD = "fold";

    // player stay
    public const string PLAYER_STAY = "stay";

    // get list of open games
    public const string GET_OPEN_GAMES = "getlist";
    public const string NO_OPEN_GAMES = "noneopen";
    public const string YES_OPEN_GAMES = "opengames";

    // get game info
    public const string GET_GAME_INFO = "getgameinfo";
    public const string RETURN_GAME_INFO = "returngameinfo";
    public static string GameInfoToString(string name, string owner, int numPlayers, string[] playerNames)
    {
        string s = "";
        s += (name + " ");
        s += (owner + " ");
        s += (numPlayers.ToString() + " ");
        for (int i = 0; i < numPlayers; i++)
        {
            if (playerNames.Length > i)
                s += playerNames[i];
            else
                s += EMPTY_STRING;
        }
        return s;
    }

    public const string EMPTY_STRING = "emptystring";
        
    #endregion
}
