## Multiplayer Poker w/ Socket Programming
An online poker game based on the basic rule set of 5-card poker. This project uses socket programming using .NET Framework System.Net.Sockets namespace implementation of Windows Sockets for connection and communication between users over a network.

## Project Details
### Server-Client Architecture
The server manages the game logic including the deck of cards, the dealer, and the player interactions. The server runs the game while communicating relevant information to each client. Clients manage the individual player information for the client including the hand of cards and player input. The clients communicate player input to the server. The server is run on a local client which acts as the host server.

### Unity Scene Flow
* The application opens on the "StartScene" giving the player 2 options to either host a new game or join an existing game.
    * When choosing "Host Game", the application becomes the host server for a new lobby that other players (clients) may join.
        * This launches the "Server-Client" scene, with a new game lobby and the host application represented as Player 1.
    * When choosing "Join Game", the application searches for existing host servers with an open lobby to join.
        * This launches the "Client" scene, with the player joined in a game lobby if one exists.
* In the game lobby, all clients must "ready up", then the server-client (or lobby leader) may start a new game of poker.